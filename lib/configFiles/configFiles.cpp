// configFile API - Jason Cox

#include "Arduino.h"
#include "configFiles.h"

String configFiles::read(String name){
    LittleFS.begin();
    String content="";
    if (LittleFS.exists(name)){
        File file = LittleFS.open(name,"r");
        if (file){
            // Serial.println( "reading file ::: " + name);
            size_t size = file.size();
            std::unique_ptr<char[]> buf(new char[size ]);
            file.readBytes(buf.get(),size + 1);
            content = buf.get();
            file.close();
        }

    } else{
        Serial.println("Files does not exist");
    }
    return content;
}

void configFiles::write(String name, String content){
    LittleFS.begin();
    // Serial.println("saving file " + name);
    File configFile = LittleFS.open(name, "w");
    if (!configFile) {
      Serial.println("failed to open config file for writing");
    }
    DynamicJsonBuffer jsonBuffer;
    JsonArray& json = jsonBuffer.parseArray(content);
    json.printTo(configFile);
    configFile.close();
    return;
}