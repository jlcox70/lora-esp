/* 
    file handle
*/
#ifndef configFiles_h
#define configFiles_h

#include "Arduino.h"
#include "LittleFS.h"
#include <ArduinoJson.h>  

class configFiles;
class configFiles {
    public:
        String read(String name);
        void  write(String name,String content);
    private:
    
};
#endif