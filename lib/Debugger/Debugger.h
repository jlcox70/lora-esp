/* 
    Debug print to serial only when set

*/
#ifndef Debugger_h
#define Debugger_h

#include "Arduino.h"

class Debugger;
class Debugger {
    public:
        Debugger(bool enable);
        void msg(String msg);
        void enable(bool enable);
    private:
        bool _enable;
};
#endif
