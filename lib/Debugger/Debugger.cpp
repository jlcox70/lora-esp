#include "Arduino.h"
#include "Debugger.h"

Debugger::Debugger( bool enable){
    _enable = enable;
}

void Debugger::enable ( bool enable){
    _enable = enable;
}
void Debugger::msg ( String Msg ){
    if ( _enable == 1 ) {
        Serial.println(Msg);
    }
}