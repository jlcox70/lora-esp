#include "Arduino.h"
#include "NMEAParser.h"
#include <string.h>

NMEAParser::NMEAParser() {}

bool NMEAParser::validateSentence(const char* sentence) {
    if (!verifyChecksum(sentence)) {
        return false;
    }
    return true;
}

int NMEAParser::getFieldCount(const char* sentence) {
    // Count the number of fields in the sentence
    int count = 0;
    char* token = strtok((char*)sentence, ",");
    while (token != NULL) {
        count++;
        token = strtok(NULL, ",");
    }
    return count;
}
String NMEAParser::getField(int index, const char* sentence) {
    // Extract the field at the specified index from the sentence

    char buffer[strlen(sentence) + 1];
    strcpy(buffer, sentence);

    char* token = strtok(buffer, ",");
    int currentField = 0;

    while (token != NULL) {
        if (currentField == index) {
            return String(token);
        }

        currentField++;
        token = strtok(NULL, ",");
    }

    // Return an empty string if the index is out of bounds
    return "";
}

bool NMEAParser::verifyChecksum(const char* sentence) {
    // Find the position of the '*' character
    const char* asteriskPos = strchr(sentence, '*');

    // Check if there's an asterisk and at least two characters after it
    if (asteriskPos == NULL || strlen(asteriskPos) < 3) {
        return false;
    }

    // Extract the checksum part from the sentence
    char checksumStr[3];
    strncpy(checksumStr, asteriskPos + 1, 2);
    checksumStr[2] = '\0';

    // Convert the checksum string to an integer
    int expectedChecksum = strtol(checksumStr, NULL, 16);

    // Calculate the checksum for the sentence (excluding the '$' character)
    int calculatedChecksum = 0;
    for (int i = 1; i < asteriskPos - sentence; i++) {
        calculatedChecksum ^= sentence[i];
    }

    return (calculatedChecksum == expectedChecksum);
}