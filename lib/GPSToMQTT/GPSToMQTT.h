// GPSToMQTT.h

#ifndef GPSTOMQTT_H
#define GPSTOMQTT_H

#include <TinyGPS++.h>
#include <ArduinoJson.h>
#include <PubSubClient.h>

class GPSToMQTT {
public:
  GPSToMQTT(TinyGPSPlus& gps, PubSubClient& mqttClient, const char* mqtt_topic, Print& telnetClient);
//   void begin();
  void loop();
  void setGPSData(float latitude, float longitude, int satellites, float hdop);

private:
    TinyGPSPlus& gps;
    PubSubClient& client;
    const char* mqtt_topic;
    float latitude;
    float longitude;
    int satellites;
    float hdop;
  Print& telnetClient; // Reference to telnetClient

    void publishData();
    void reconnect();
    bool runEvery(unsigned long interval);
    double haversine(double lat1, double lon1, double lat2, double lon2);
};

#endif
