// GPSToMQTT.cpp

#include "GPSToMQTT.h"
#include <cmath>
uint ignore = 5;
uint count = 0;
GPSToMQTT::GPSToMQTT(TinyGPSPlus &gps, PubSubClient &mqttClient, const char *mqtt_topic, Print &telnetClient)
    : gps(gps), client(mqttClient), mqtt_topic(mqtt_topic), telnetClient(telnetClient)
{
}
void GPSToMQTT::loop()
{
  // publishData();

  // Reconnect to MQTT broker if necessary
  if (!client.connected())
  {
    reconnect();
  }

  // Keep the MQTT connection alive
  // client.loop();
  if (runEvery(5000))
  {
    publishData();
  }
}
boolean GPSToMQTT::runEvery(unsigned long interval)
{
  static unsigned long previousMillis = 0;
  unsigned long currentMillis = millis();
  if (currentMillis - previousMillis >= interval)
  {
    previousMillis = currentMillis;
    return true;
  }
  return false;
}
void GPSToMQTT::publishData()
{
  static const double HOME_LAT = -32.71511917, HOME_LON = 151.65101;
double distanceKmToHOME = GPSToMQTT::haversine(gps.location.lat(), gps.location.lng(), HOME_LAT, HOME_LON);
  
  double courseToHOME =
      TinyGPSPlus::courseTo(
          gps.location.lat(),
          gps.location.lng(),
          HOME_LAT,
          HOME_LON);
  // build date
  String dateStr = "";

  if (gps.date.isValid())
  {
    dateStr += String(gps.date.month());
    dateStr += "/";
    dateStr += String(gps.date.day());
    dateStr += "/";
    dateStr += String(gps.date.year());
  }
  String timeStr = "";

  if (gps.time.isValid())
  {
    if (gps.time.hour() < 10)
      timeStr += "0";
    timeStr += String(gps.time.hour());
    timeStr += ":";
    if (gps.time.minute() < 10)
      timeStr += "0";
    timeStr += String(gps.time.minute());
    timeStr += ":";
    if (gps.time.second() < 10)
      timeStr += "0";
    timeStr += String(gps.time.second());
    timeStr += ".";
    if (gps.time.centisecond() < 10)
      timeStr += "0";
    timeStr += String(gps.time.centisecond());
  }
  // Create a JSON object
  DynamicJsonDocument jsonDoc(256); // Adjust the size based on your data

  // Add data fields to the JSON object
  jsonDoc["sats"] = gps.satellites.value();
  jsonDoc["hdop"] = gps.hdop.hdop();
  jsonDoc["latitude"] = gps.location.lat();
  jsonDoc["longitude"] = gps.location.lng();
  jsonDoc["speed"] = gps.speed.kmph();
  // Bearings;
  jsonDoc["distance to home klm"] = distanceKmToHOME;
  jsonDoc["course to home"] = courseToHOME;
  // Add more data fields as needed
  jsonDoc["fix"] = gps.location.age();
  jsonDoc["date"] = dateStr;
  jsonDoc["time"] = timeStr;
  ;
  // Add more data fields as needed
  jsonDoc["chars"] = gps.charsProcessed();
  jsonDoc["sentences"] = gps.sentencesWithFix();
  jsonDoc["checksum"] = gps.failedChecksum();

  // Serialize the JSON object to a string
  String jsonString;
  serializeJson(jsonDoc, jsonString);

    // Publish the JSON string to the MQTT topic
    client.publish(mqtt_topic, jsonString.c_str());
  
}

void GPSToMQTT::reconnect()
{
  while (!client.connected())
  {
    telnetClient.println("Attempting MQTT connection...");
    if (client.connect("ESP8266Client"))
    {
      telnetClient.println("Connected to MQTT broker");
    }
    else
    {
      telnetClient.print("Failed, rc=");
      telnetClient.print(client.state());
    }
  }
}
double GPSToMQTT::haversine(double lat1, double lon1, double lat2, double lon2) {
    // Convert latitude and longitude from degrees to radians
    lat1 = lat1 * M_PI / 180.0;
    lon1 = lon1 * M_PI / 180.0;
    lat2 = lat2 * M_PI / 180.0;
    lon2 = lon2 * M_PI / 180.0;

    // Haversine formula
    double dlat = lat2 - lat1;
    double dlon = lon2 - lon1;
    double a = sin(dlat / 2) * sin(dlat / 2) + cos(lat1) * cos(lat2) * sin(dlon / 2) * sin(dlon / 2);
    double c = 2 * atan2(sqrt(a), sqrt(1 - a));

    // Radius of the Earth in kilometers (mean value)
    const double R = 6371.0;

    // Calculate the distance
    double distance = R * c;

    return distance;
}