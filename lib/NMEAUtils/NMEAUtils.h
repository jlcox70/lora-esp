#ifndef NMEA_UTILS_H
#define NMEA_UTILS_H

#include <NMEAParser.h>
#include <Arduino.h>  // Assuming Arduino environment for String class

class NMEAUtils {
  public:
    NMEAUtils();  // Constructor to initialize internal variables

    void processNMEASentences(const String& input);
    String findAndProcessSentence(const String& input, const String& sentenceType);
    unsigned long calculateEpochTime(const String& gprmcSentence);  // Made static
    unsigned long getCurrentCalculatedTime(const String& gprmcSentence);

  private:
    NMEAParser parser;
    static unsigned long lastGPSTime;
    unsigned long lastReadTime;
    unsigned long lastReadTimeEpoc;
    int secondsSinceLastRead;
    unsigned long calculateEpochTimeInternal(const String& gprmcSentence);  // Helper function

};

#endif  // NMEA_UTILS_H
