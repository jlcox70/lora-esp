#include "NMEAUtils.h"
#include "NMEAParser.h"


unsigned long NMEAUtils::lastGPSTime = 0;  // Initialization for static member

NMEAUtils::NMEAUtils() : lastReadTime(0), secondsSinceLastRead(0) {
  // Constructor to initialize internal variables
}

void NMEAUtils::processNMEASentences(const String& input) {
  int length = input.length();
  for (int i = 0; i < length; ++i) {
    char c = input[i];
    if (c == '$') {
      // Start of a new NMEA sentence
      String sentence = "";
      while (i < length && c != '\n') {
        c = input[i++];
        sentence += c;
      }
      // Process the complete NMEA sentence
      if (parser.validateSentence(sentence.c_str())) {
        // Do something with the parsed data
        // For example, access individual fields using parser.getField()
        Serial.println("Parsed sentence: " + sentence);
        if (sentence.startsWith("$GPRMC")) {
          lastGPSTime = calculateEpochTime(sentence);
          Serial.println("Epoch time from GPRMC: " + String(lastGPSTime));
        }
      }
    }
  }
}

String NMEAUtils::findAndProcessSentence(const String& input, const String& sentenceType) {
  int index = input.indexOf(sentenceType);
  while (index != -1) {
    String sentence = input.substring(index, input.indexOf('\n', index));
    if (parser.validateSentence(sentence.c_str())) {
      // Do something with the parsed data
      // For example, access individual fields using parser.getField()
      Serial.println("Found and parsed sentence: " + sentence);
      if (sentence.startsWith("$GPRMC")) {
        lastGPSTime = calculateEpochTime(sentence);
        Serial.println("Epoch time from GPRMC: " + String(lastGPSTime));
      }
      return sentence;
    }
    index = input.indexOf(sentenceType, index + 1);
  }
  return "";
}

unsigned long NMEAUtils::calculateEpochTime(const String& gprmcSentence) {
  return calculateEpochTimeInternal(gprmcSentence);
}

unsigned long NMEAUtils::calculateEpochTimeInternal(const String& gprmcSentence) {
  // Extract time and date fields from the GPRMC sentence
  String timeField = parser.getField(1, gprmcSentence.c_str());
  String dateField = parser.getField(9, gprmcSentence.c_str());

  // Extract individual time components
  int hour = timeField.substring(0, 2).toInt();
  int minute = timeField.substring(2, 4).toInt();
  int second = timeField.substring(4, 6).toInt();

  // Extract individual date components
  int day = dateField.substring(0, 2).toInt();
  int month = dateField.substring(2, 4).toInt();
  int year = dateField.substring(4, 6).toInt() + 2000;

  // Calculate epoch time
  struct tm tmTime;
  tmTime.tm_year = year - 1900;
  tmTime.tm_mon = month - 1;
  tmTime.tm_mday = day;
  tmTime.tm_hour = hour;
  tmTime.tm_min = minute;
  tmTime.tm_sec = second;

  // Calculate epoch time
  if (mktime(&tmTime) == -1) {
    // Handle parsing error, e.g., throw an exception or return a special value
    return 0;
  }
  return mktime(&tmTime);
}

unsigned long NMEAUtils::getCurrentCalculatedTime(const String& gprmcSentence) {

unsigned long currentTime = millis() / 1000;

  if (gprmcSentence.length() > 0) {
    // GPRMC sentence is valid, calculate epoch time from GPRMC
    lastReadTimeEpoc = calculateEpochTimeInternal(gprmcSentence);
    lastReadTime = currentTime;
    secondsSinceLastRead = 0;
  } else if (lastGPSTime != 0) {
    // No new sentence, use the last known good GPS time
    secondsSinceLastRead = currentTime - lastReadTime;
  } else {
    // No sentence and no last known good GPS time, return 0 or handle accordingly
    return 0;
  }

  // Update lastReadTime to the current time
  // lastReadTime = currentTime;

  // Return the current calculated time
  return lastReadTimeEpoc + secondsSinceLastRead;
}
