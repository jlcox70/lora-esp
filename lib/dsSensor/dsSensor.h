#ifndef dsSensor_h
#define dsSensor_h

#include "Arduino.h"
#include <DallasTemperature.h>
#include <OneWire.h>

class dsSensor;
class dsSensor {
    public:
        dsSensor(int bus_pin);

        String read(String name);
        void printAddress( DeviceAddress deviceAddress);
        void printTempurature(DeviceAddress deviceAddress);
        void printData(DeviceAddress deviceAddress);

    private:
        int _bus_pin;
        int device_count = 0;
        DallasTemperature sensors;
        // OneWire oneWire(_bus_pin);
        // DallasTemperature sensors(&oneWire);
        // DeviceAddress Thermometer;
};
#endif