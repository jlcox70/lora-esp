#include "Arduino.h"
#include "dsSensor.h"

// OneWire oneWire(_bus_pin);
// DallasTemperature sensors(&oneWire);
// DeviceAddress Thermometer;

dsSensor::dsSensor( int bus_pin){
    _bus_pin = bus_pin;
}

// function to print a device address
void dsSensor::printAddress(DeviceAddress deviceAddress)
{
  for (uint8_t i = 0; i < 8; i++)
  {
    if (deviceAddress[i] < 16)
      Serial.print("0");
    Serial.print(deviceAddress[i], HEX);
  }
}
void dsSensor::printTempurature(DeviceAddress deviceAddress)
{
  float tempC = sensors.getTempC(deviceAddress);
  Serial.print("Temp C: ");
  Serial.print(tempC);
  Serial.print(" Temp F: ");
  Serial.print(DallasTemperature::toFahrenheit(tempC));
}

void dsSensor::printData(DeviceAddress deviceAddress)
{
  Serial.print("Device Address: ");
  printAddress(deviceAddress);
  Serial.print(" ");
  dsSensor::printTempurature(deviceAddress);
  Serial.println();
}