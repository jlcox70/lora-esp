// Sensor API - Jason Cox

#include "Arduino.h"
#include "SensorAPI.h"




SensorAPI::SensorAPI (String host){
  _http_url = host;
}

// Update the status field for the sensor
int SensorAPI::update_status(int id, int status){
//  delay(1000);
  // String _http_url;
  WiFiClientSecure client;
  client.setInsecure();
  HTTPClient statusHttp;
  String statusData = "status=" + String(status) ;
  String put_url = _http_url + "appliance/status/"+ String(id);
  // Serial.println(" update_status url : " + put_url);
  statusHttp.begin(client,put_url);
  statusHttp.setTimeout(10000);
  statusHttp.addHeader("Content-Type", "application/x-www-form-urlencoded");  
  int httpCode = statusHttp.PUT(statusData);
  statusHttp.end();
  return httpCode ;
}

// Update the version field for the sensor
int SensorAPI::update_version(int id, String version){
  //delay(1000);
  // String _http_url;
  WiFiClientSecure client;
  client.setInsecure();
  HTTPClient statusHttp;
  String statusData = "version=" + String(version) ;
  String put_url = _http_url + "appliance/version/"+ String(id);
  // Serial.println(" update_status url : " + put_url);
  statusHttp.begin(client,put_url);
  statusHttp.setTimeout(10000);
  statusHttp.addHeader("Content-Type", "application/x-www-form-urlencoded");  
  int httpCode = statusHttp.PUT(statusData);
  statusHttp.end();
  return httpCode ;
}

// Determine the ID of a given 
int SensorAPI::getId ( String payload){
  DynamicJsonBuffer jsonBuffer;
  JsonArray& raw_ByMac = jsonBuffer.parseArray(payload);
  String id = raw_ByMac[0]["id"];
  return id.toInt();
}

// retrun the json object for the apo
String SensorAPI::get_appliance_payload (String Mac){
  WiFiClientSecure client;
  client.setInsecure();
  HTTPClient http;
  String data = "mac=" + Mac;
  String post_url = _http_url + "appliance/mac";
  http.begin(client,post_url);
  http.setTimeout(10000);
  http.addHeader("Content-Type", "application/x-www-form-urlencoded");  
  int httpCode = http.POST(data);
  String appliancePayload = http.getString();                  //Get the response payload
  http.end();
  return appliancePayload;
}

// Register a new device
void SensorAPI::register_device(String Mac){
  WiFiClientSecure client;
  client.setInsecure();
  HTTPClient http;
  // Post mac= <mac add>
  String data = "mac=" + Mac;
  // Serial.println("create new DB entry for Mac :" + data );
  String post_url = _http_url + "appliance";
  // Serial.println(post_url);
  http.begin(client, post_url);
  http.setTimeout(10000);
  http.addHeader("Content-Type", "application/x-www-form-urlencoded");  
  int httpCode = http.POST(data);
  String payload = http.getString();                  //Get the response payload
  // Serial.println(httpCode);   //Print HTTP return code
  // Serial.println(payload);    //Print request response payload
  http.end();
  return;
}

// get appliance Status
int SensorAPI::get_status( String payload){
  DynamicJsonBuffer jsonBuffer;
  JsonArray& status_raw = jsonBuffer.parseArray(payload);
  int status = status_raw[0]["status"];
  return status;
}
// Get debug setting
// Determine the ID of a given 
String SensorAPI::getDebug( String payload){
  DynamicJsonBuffer jsonBuffer;
  JsonArray& raw_ByMac = jsonBuffer.parseArray(payload);
  String debug = raw_ByMac[0]["debug"];
  return debug;
}

// Determine the Name of a given device
String SensorAPI::getName( String payload){
  DynamicJsonBuffer jsonBuffer;
  JsonArray& raw_ByMac = jsonBuffer.parseArray(payload);
  String name = raw_ByMac[0]["name"];
  return name;
}
// Determine the Name of a given device
String SensorAPI::getDescription( String payload){
  DynamicJsonBuffer jsonBuffer;
  JsonArray& raw_ByMac = jsonBuffer.parseArray(payload);
  String description = raw_ByMac[0]["description"];
  return description;
}

// Get version from DB
String SensorAPI::getVersion ( String payload){
  DynamicJsonBuffer jsonBuffer;
  JsonArray& raw_ByMac = jsonBuffer.parseArray(payload);
  String version = raw_ByMac[0]["version"];
  return version;
}
// Get lastUpdate from DB
String SensorAPI::getLastUpdate ( String payload){
  DynamicJsonBuffer jsonBuffer;
  JsonArray& raw_ByMac = jsonBuffer.parseArray(payload);
  String updated_at = raw_ByMac[0]["updated_at"];
  return updated_at;
}

// register a sensor
void SensorAPI::registerSensor(int device_id, String sensor_id){
  WiFiClientSecure client;
  client.setInsecure();
  
  HTTPClient http;
  // Post mac= <mac add>
  String applianceData = "appliance_id="+String (device_id);
  String deviceData = "&device_id="+sensor_id;
  String data = applianceData + deviceData;
  // Serial.println("create new Sensor :" + data );
  String post_url = _http_url + "sensor";
  // Serial.println(post_url);
  http.begin(client,post_url);
  http.setTimeout(10000);
  http.addHeader("Content-Type", "application/x-www-form-urlencoded");  
  int httpCode = http.POST(data);
  String payload = http.getString();                  //Get the response payload
  // Serial.println(httpCode);   //Print HTTP return code
  // Serial.println(payload);    //Print request response payload
  http.end();
  return;
}

// search for sensor Id by device Id and in mapping json
String SensorAPI::getSensorById (String sensor_id, String payload){
  // Serial.println ("getSensorById : " + payload);
  DynamicJsonBuffer jsonBuffer;
  JsonArray& raw = jsonBuffer.parseArray(payload);
  // now loop on array and return if matched the object
  int i;
  for ( i=0; i < sizeof(raw)+1; i++){
    if ( raw[i]["device_id"] == sensor_id ){
      return raw[i];
    }
  }
  return "nil";
}
// search for sensor Id by device Id and in mapping json
int SensorAPI::getSensorId ( String payload){
  // Serial.println("getSensorId payload :" + payload);
  DynamicJsonBuffer jsonBuffer;
  JsonObject& raw = jsonBuffer.parseObject(payload);
  return raw["id"];
}

// insert vaule for sensor
void SensorAPI::insertSensorValue ( String id, String value){
   WiFiClientSecure client;
  client.setInsecure();
  
  HTTPClient http;
  String data = "value=" + value;
  // Serial.println("create new Sensor :" + data );
  String post_url = _http_url + "sensors/update/" + id;
  // Serial.println(post_url);
  http.begin(client,post_url);
  http.setTimeout(10000);
  http.addHeader("Content-Type", "application/x-www-form-urlencoded");  
  int httpCode = http.POST(data);
  String payload = http.getString();                  //Get the response payload
  // Serial.println(httpCode);   //Print HTTP return code
  // Serial.println(payload);    //Print request response payload
  http.end();
  return;
}

// retrun the json object for the apo
String SensorAPI::get_mapping_payload (int id){
  WiFiClientSecure client;
  client.setInsecure();
  HTTPClient http;
  String url = _http_url + "sensors/" + id;
  http.begin(client,url);
  http.setTimeout(10000);
  int httpCode = http.GET();
  String mappingPayload = http.getString();                  //Get the response payload
  http.end();
  return mappingPayload;
}

// Determine the power mode
bool SensorAPI::get_lowPower ( String payload){
  DynamicJsonBuffer jsonBuffer;
  JsonArray& raw_ByMac = jsonBuffer.parseArray(payload);
  bool id = raw_ByMac[0]["lowPower"];
  return id;
}

// Determine the time to sleep for 
int SensorAPI::get_updateSeconds ( String payload){
  DynamicJsonBuffer jsonBuffer;
  JsonArray& raw_ByMac = jsonBuffer.parseArray(payload);
  String id = raw_ByMac[0]["updateSeconds"];
  return id.toInt();
}