#ifndef SensorAPI_h
#define SensorAPI_h

#include <Arduino.h>
#include <ESP8266WiFi.h>
#include <WiFiClient.h>
#include <WiFiUdp.h>
#include <ESP8266HTTPClient.h>
#include <ArduinoJson.h>  

class SensorAPI;
class SensorAPI {
     public:
        SensorAPI( String http_url );
        void set_host(String http_url);
        int update_status(int id,int status);
        int get_status ( String payload );
        int update_version(int id,String version);
        int getId ( String payload);
        String get_appliance_payload (String Mac);
        void register_device(String Mac);
        String getDebug (String payload);
        String getName(String payload);
        String getDescription(String payload);
        String getVersion(String payload);
        String getLastUpdate ( String payload);

        void registerSensor(int device_id, String sensor_id);
        String getSensorById(  String sensor_id, String payload);
        int getSensorId(  String payload);

        void insertSensorValue( String id, String value);

        String get_mapping_payload ( int device_id);
        bool get_lowPower(String payload);
        int get_updateSeconds(String payload);
    
    private:
        String _http_url;
        String _payload;
       
};
#endif