#include "Arduino.h"
#include "fwUpdate.h"

void fwUpdate::debug(bool debug){
    _debug = debug;
}



void fwUpdate::update(String url, String version){
    WiFiClientSecure client;
    client.setInsecure();
    signPubKey = new BearSSL::PublicKey(signing_pubkey);
    hash = new BearSSL::HashSHA256();
    sign = new BearSSL::SigningVerifier(signPubKey);
    client.setTimeout(10);
    Update.installSignature(hash, sign);
    Serial.println("In Update....");
    Serial.println("URL :" + url);
    Serial.println( "Version :" + version);
    ESPhttpUpdate.setLedPin(LED_BUILTIN, LOW);
    // t_httpUpdate_return ret = ESPhttpUpdate.update("http://jason:foo@firmware.globelock.org/file.php",VERSION_SHORT);
    t_httpUpdate_return ret = ESPhttpUpdate.update(client, url ,version);
    // t_httpUpdate_return ret = ESPhttpUpdate.update(*client, "jason:foo@fw-download.globelock.home", 443, "/file.php",VERSION_SHORT);
    switch(ret) {
        case HTTP_UPDATE_FAILED:
        if ( _debug == true ){
            Serial.println("HTTP_UPDATE_FAILD");
        };
        break;
        case HTTP_UPDATE_NO_UPDATES:
        if ( _debug == true ){
            Serial.println("HTTP_UPDATE_NO_UPDATES");
        };
        break;
        case HTTP_UPDATE_OK:
        if ( _debug == true ){
            Serial.println("HTTP_UPDATE_OK");
        };
        break;
    }
    return;

}