#ifndef fwUpdate_h
#define fwUpdate_h

#include "Arduino.h"
// #include "Updater_Signing.h"
#include "ESP8266WiFi.h"
#include "WiFiClient.h"
#include "WiFiUdp.h"
#include "ESP8266HTTPClient.h"
#include "ESP8266httpUpdate.h"
#include "debug.h"
#include "version.h"
#include "../../include/signing.h"


class fwUpdate;
class fwUpdate {
    public:
        void debug( bool debug );
        void update(String url,String current_version);
        
    private:
        void debug_msg( String msg);
        bool _debug;
        // const char signing_pubkey[];
        // code signing
        BearSSL::PublicKey *signPubKey = nullptr;
        BearSSL::HashSHA256 *hash;
        BearSSL::SigningVerifier *sign;
};
#endif