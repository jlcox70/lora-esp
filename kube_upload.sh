#!/bin/bash
version_raw=`grep VERSION_SHORT include/version.h  |grep v |cut -d\" -f2 `
project=`echo $version_raw |awk -F-v '{print $1}'`
version=`echo $version_raw |awk -F-v '{print "v"$2}'`


# curl  -k -F 'the_file=@.pio/build/d1/firmware.bin' http://jason:foo@firmware.globelock.org/upload.php -F "name=${project}-${version}.bin"
echo curl  -k -F 'the_file=@.pio/build/d1/firmware.bin' https://jason:foo@firmware.globelock.home/upload.php -F "name=${project}-${version}.bin"
echo Deployed: $version_raw
echo
echo
exit

kubectl cp .pio/build/d1/firmware.bin -n firmware-servers fw-7544bd7bbd-tb4xr:/var/www/html/${project}/${version}.bin
echo -n `grep VERSION_SHORT include/version.h  |grep v |cut -d\" -f2 `> VERSION_SHORT
kubectl cp VERSION_SHORT -n firmware-servers fw-7544bd7bbd-tb4xr:/var/www/html/${project}/VERSION_LATEST
echo Deployed: $version_raw