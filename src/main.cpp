/**
 * httpUpdate.ino
 *
 *  Created on: 27.11.2015
 *
 */
extern "C"
{
#include "user_interface.h" // this is for the RTC memory read/write functions
}
#include <Arduino.h>
// #include "signing.h"

// Wifi
#include <ESP8266WiFi.h>
#include <ESP8266WiFiMulti.h>
#include <WiFiManager.h>

// OTA HTTP update
#include <ESP8266HTTPClient.h>
#include <ESP8266httpUpdate.h>
#include <version.h>
#include <WiFiClientSecureBearSSL.h>

#include "NMEAParser.h"
#include "NMEAUtils.h"
#include "GPSToMQTT.h"
#include <unordered_map>
#include <TinyGPSPlus.h>
#include <time.h>
#include <Adafruit_SleepyDog.h>

// Debug Msg
#include "Debugger.h"
#define DEBUG_ESP_HTTP_CLIENT = 1
Debugger Debug = Debugger(0);

// OTA Updates
#include "ota_update_conf.h"
#include "fwUpdate.h"
fwUpdate FWUpdate;
// read batt vcc
#define VCC_ADJ 1.096 / 10
ADC_MODE(ADC_VCC);

const int ESP_LED = 14;
uint8_t ESP_LED_STATE;
ESP8266WiFiMulti wifiMulti; // Create an instance of the ESP8266WiFiMulti class, called 'wifiMulti'
WiFiClient client;
const int serialBaudRate = 115200;
const int telnetPort = 23; // Telnet default port
String telnetData;         // String to accumulate Telnet data
String serialData;         // String to accumulate Serial data
enum TelnetState
{
  WAITING,
  RECEIVING,
};
TelnetState telnetState = WAITING;

WiFiServer telnetServer(telnetPort);
WiFiClient telnetClient;
// Time to sleep (in seconds):
const int sleepTimeS = 60;

// code signing
BearSSL::PublicKey *signPubKey = nullptr;
BearSSL::HashSHA256 *hash;
BearSSL::SigningVerifier *sign;
TinyGPSPlus gps;

time_t epochTime;
char serialChar;
String inputString;
// Replace with your MQTT credentials
const char *mqtt_server = "192.168.1.102";
const char *mqtt_username = "jasoncox70";
const char *mqtt_password = "e0818733bf2e4a168db87789a2c7e78c";
const char *mqtt_topic = "gps/status";
const char *lora_mqtt_topic = "lora/raw";
const char *loraMQTTTopic;
const char *MQTTTopic ;
WiFiClient wifiClient;

PubSubClient mqttClient(wifiClient);
// GPSToMQTT gpsToMqtt(gps, mqttClient, mqtt_topic, telnetClient);

String LoRaString;
NMEAUtils nmeaUtils; // Create an instance of NMEAUtils
String dynamicMqttTopic;
String dynamicLoraMqttTopic;
// Custom hash function for the String class
struct StringHash
{
  std::size_t operator()(const String &s) const
  {
    std::hash<const char *> hash_fn;
    return hash_fn(s.c_str());
  }
};
// Define a map to associate the first word with a function or action
std::unordered_map<String, void (*)(), StringHash> actionMap;
void initializeActionMap()
{
  // Initialize the map with the desired actions
  actionMap["foo"] = []()
  {
    Debug.msg("got foo :)");
    telnetClient.print("bar\r\n");
  };
  actionMap["upgrade"] = []()
  {
    Debug.msg("got upgrade :)");
    FWUpdate.update(OTA_URL, VERSION_SHORT);
  };
  actionMap["version"] = []()
  {
    Debug.msg(VERSION);
  };
  // Add more mappings as needed
}

void check_debug()
{
  Debug.msg("Debug is true");
  Debug.enable(1);
}

String wake_status()
{
  rst_info *rsti;
  rsti = ESP.getResetInfoPtr();
  String reason ="";
  switch (rsti->reason)
  {

  case 5:
    reason =" from RTC-RESET (ResetInfo.reason = 5)";
    break;
  case 6:
    reason= " from POWER-UP (ResetInfo.reason = 6)";
    break;
  }
  reason = "reason " + rsti->reason;
  return reason;
}

void esp_led_update()
{
  if (ESP_LED_STATE == 0)
  {
    ESP_LED_STATE = 1;
    digitalWrite(ESP_LED, 1);
    Debug.msg("on");
  }
  else
  {
    ESP_LED_STATE = 0;
    digitalWrite(ESP_LED, 0);
    Debug.msg("off");
  }
}

void setup()
{
  Serial.begin(115200);
  Serial.print("in setup");
  

  // Setup esp TinyGPSPlus gps;led
  pinMode(ESP_LED, OUTPUT);
  ESP_LED_STATE = 0;
  esp_led_update();
  delay(500);
  esp_led_update();
  delay(500);

  WiFiManager wifiManager;
  wifiManager.setDebugOutput(true);
  wifiManager.setConnectTimeout(60);
  wifiManager.setConfigPortalTimeout(60);
  wifiManager.autoConnect("Globelock-IoT");

  Serial.printf("Project: %s", VERSION_SHORT);
  Serial.println(wake_status());
  Debug.enable(1);

  Debug.msg(" ");
  FWUpdate.debug(true);
  FWUpdate.update(OTA_URL, VERSION_SHORT);
  Debug.msg(VERSION);
  Debug.msg("Exit Setup.....");
  // Start Telnet server
  telnetServer.begin();
  telnetServer.setNoDelay(true);
  // Initialize the action map
  initializeActionMap();
  // Set up MQTT
  mqttClient.setServer(mqtt_server, 1883);
  mqttClient.connect("gps", mqtt_username, mqtt_password);
  // Get the MAC address
  String macAddress = WiFi.macAddress();

  // Create dynamic topics with MAC address
  dynamicMqttTopic = mqtt_topic;
  dynamicMqttTopic += "/";
  dynamicMqttTopic += macAddress;
  dynamicLoraMqttTopic = lora_mqtt_topic;
  dynamicLoraMqttTopic += "/";
  dynamicLoraMqttTopic += macAddress;
  String willTopic = dynamicMqttTopic;
  willTopic += "/will";
  loraMQTTTopic = dynamicLoraMqttTopic.c_str();
  MQTTTopic = dynamicMqttTopic.c_str();
  mqttClient.publish("debug",loraMQTTTopic);
  mqttClient.publish("debug",MQTTTopic);
  mqttClient.publish(willTopic.c_str(),wake_status().c_str());
// setup watchdog
  int countdownMS = Watchdog.enable(4000);

    system_get_rst_info();

}

void processTelnetData()
{
  // Process the received Telnet data here
  Debug.msg("in processData with:" + telnetData + ":");

  // Split the string into words
  String firstWord = telnetData.substring(0, telnetData.indexOf(' '));
  Debug.msg(firstWord);
  firstWord.trim();
  // Convert firstWord to lowercase for case-insensitive comparison
  String lowerFirstWord = firstWord;
  lowerFirstWord.toLowerCase();

  // Compare with lowercase versions of the keys in actionMap
  for (const auto &pair : actionMap)
  {
    String lowerKey = pair.first;
    lowerKey.toLowerCase();

    if (lowerKey == lowerFirstWord)
    {
      pair.second(); // Call the corresponding function
      return;
    }
  }

  if (actionMap.find(firstWord) != actionMap.end())
  {
    actionMap[firstWord]();
  }
  else
  {
    Debug.msg("Unknown action: " + firstWord);
  }
  telnetData = "";
}

void displayInfo()
{
  telnetClient.print(F("Location: "));
  if (gps.location.isValid())
  {
    telnetClient.print(gps.location.lat(), 6);
    telnetClient.print(F(","));
    telnetClient.print(gps.location.lng(), 6);
  }
  else
  {
    telnetClient.print(F("INVALID"));
  }
  telnetClient.print(F("  HDOP: "));
  if (gps.hdop.isValid())
  {
    telnetClient.print(gps.hdop.hdop());
  }
  else
  {
    telnetClient.print(F("INVALID"));
  }
  telnetClient.print(F("  Date/Time: "));
  if (gps.date.isValid())
  {
    telnetClient.print(gps.date.month());
    telnetClient.print(F("/"));
    telnetClient.print(gps.date.day());
    telnetClient.print(F("/"));
    telnetClient.print(gps.date.year());
  }
  else
  {
    telnetClient.print(F("INVALID"));
  }

  telnetClient.print(F(" "));
  if (gps.time.isValid())
  {
    if (gps.time.hour() < 10)
      telnetClient.print(F("0"));
    telnetClient.print(gps.time.hour());
    telnetClient.print(F(":"));
    if (gps.time.minute() < 10)
      telnetClient.print(F("0"));
    telnetClient.print(gps.time.minute());
    telnetClient.print(F(":"));
    if (gps.time.second() < 10)
      telnetClient.print(F("0"));
    telnetClient.print(gps.time.second());
    telnetClient.print(F("."));
    if (gps.time.centisecond() < 10)
      telnetClient.print(F("0"));
    telnetClient.print(gps.time.centisecond());
  }
  else
  {
    telnetClient.print(F("INVALID"));
  }

  telnetClient.println();
  struct tm tmTime;
  tmTime.tm_mon = gps.date.month() - 1;
  tmTime.tm_mday = gps.date.day();
  tmTime.tm_year = gps.date.year() - 1900;
  tmTime.tm_hour = gps.time.hour();
  tmTime.tm_min = gps.time.minute();
  tmTime.tm_sec = gps.time.second();
  epochTime = mktime(&tmTime);
  telnetClient.print("epoc : ");
  telnetClient.println(epochTime);

  // GPSToMQTT gpsToMqtt(gps, mqttClient, MQTTTopic, telnetClient);
GPSToMQTT gpsToMqtt(gps, mqttClient, mqtt_topic, telnetClient);

  gpsToMqtt.loop();
}

void handleSerial()
{

  while (Serial.available() > 0)
  {
    char serialChar = Serial.read(); // Read a character from the serial input
    if (serialChar != '\r' && serialChar != '\n')
    {
      if (inputString.length() > 3 && serialChar =='$'){
        inputString = serialChar;
      }

      inputString += serialChar; // Append the character to the inputString
    }
    if (gps.encode(serialChar))
    {
      displayInfo();
      inputString.replace("$$", "$");
      mqttClient.publish(loraMQTTTopic, inputString.c_str());
      inputString = "";
    }
  }

  // if (millis() > 5000 && gps.charsProcessed() < 10)
  // {
  //   Serial.println(F("No GPS detected: check wiring."));
  //   while (true)
  //     ;
  // }
}

void handleTelnet()
{
  if (telnetServer.hasClient())
  {
    if (!telnetClient || !telnetClient.connected())
    {
      if (telnetClient)
      {
        telnetClient.stop();
      }
      telnetClient = telnetServer.available();
      Serial.println("Telnet client connected");
      esp_led_update();
    }
    while (telnetClient.available())
    {
      // telnetData = telnetClient.readStringUntil('\r');
      char telnetChar = telnetClient.read();

      // Filter out Telnet negotiation strings
      if (telnetChar == '\xFF' || telnetChar == '\xF6')
      {
        // Skip Telnet control characters
        continue;
      }
      // Remove line endings from telnetChar
      if (telnetChar != '\r' && telnetChar != '\n')
      {
        telnetData += telnetChar;
      }
      else if (telnetChar >= 32 && telnetChar <= 126)
      {
        // Filter out non-printable ASCII characters
        telnetData += telnetChar;
      }

      // Process Telnet data when a line ending is received
      if (telnetChar == '\r' || telnetChar == '\n')
      {
        Debug.msg("telnet: " + telnetData);
        processTelnetData();
        telnetData = ""; // Clear the accumulated Telnet data after processing
      }

      // Debug.msg("telnet: " + telnetData);
      // processTelnetData();
    }
    esp_led_update();
  }
}

void loop()
{
  Watchdog.reset();

  handleSerial();
  handleTelnet();
}