# !/bin/bash
# set -x
# Colors & helpers
GREEN="\033[0;32m"
RED="\033[0;31m"
CLEAR="\033[0m"

green() { echo -e "${GREEN}"$@"${CLEAR}"; }
red() { >&2 echo -e "${RED}"$@"${CLEAR}"; }

# Binary paths
SIGNING_BIN="$HOME/.platformio/packages/framework-arduinoespressif8266/tools/signing.py"
UPLOADER_BIN="$HOME/.platformio/packages/framework-arduinoespressif8266/tools/espota.py"
chmod +x $SIGNING_BIN
# Parse arguments
while (( "$#" )); do
  case "$1" in
    --ota-sign-private)
      OTA_PRIVKEY=$2
      shift 2
      ;;
    --upload-built-binary)
      OTA_BIN=$2
      shift 2
      ;;
    *)
      BYPASS_PARAMS="$BYPASS_PARAMS $1"
      shift
      ;;
  esac
done

# Check existence and permissions on signing.py script from pio package
if ! [[ -x $SIGNING_BIN ]]
then
    red "[SIGN] signing.py is not found or it is not executable\n"
    exit 1
fi

# Check existence and permissions on espota.py script from pio package
# if ! [[ -x $UPLOADER_BIN ]]
# then
#     red "[SIGN] espota.py is not found or it is not executable\n"
#     exit 1
# fi

# Create temp signed file
SIGNED_FILE_TMP=`mktemp --dry-run`

# Ahh, this if statement is awful, because signing script's developers decided not to exit with code 1 in case of signing failure idk why :(
# https://github.com/esp8266/Arduino/blob/master/doc/ota_updates/readme.rst#automatic-signing----only-available-on-linux-and-mac
# Moreover, signing.py is also puts the "successfull signed" message into stderr
echo Signing $OTA_BIN
# gzip -9 $OTA_BIN -f
ls -l ${OTA_BIN}*
SIGN_OUT=$($SIGNING_BIN --mode=sign --bin ${OTA_BIN} --out $SIGNED_FILE_TMP --privatekey $OTA_PRIVKEY  2>&1 >/dev/null)

if [[ "$SIGN_OUT" != "Signed binary: $SIGNED_FILE_TMP" ]];
then
  red "[SIGN] Signing script failed, please ensure that your keys are readable and upload_flags are correct"
  exit 1
fi

green "[SIGN] Firmware binary successfully signed (^・x・^), uploading to server..."

# Call espota.py then remove signed file
#($UPLOADER_BIN -f $SIGNED_FILE_TMP $BYPASS_PARAMS && rm $SIGNED_FILE_TMP) || (rm $SIGNED_FILE_TMP; exit 1)
version_raw=`grep VERSION_SHORT include/version.h  |grep v |cut -d\" -f2 `
project=`echo $version_raw |awk -F-v '{print $1}'`
version=`echo $version_raw |awk -F-v '{print "v"$2}'`
# project=`echo $version_raw |cut -d- -f1`
# version=`echo $version_raw |cut -d- -f2`

ls -l ${SIGNED_FILE_TMP}
# curl  -k -F "the_file=@${SIGNED_FILE_TMP}" http://jason:foo@127.0.0.1:8080/upload -F "name=${project}-${version}"
# curl  -k -F "the_file=@${SIGmultipart/form-dataNED_FILE_TMP}" https://jason:foo@firmware.globelock.org/upload.php -F "name=${project}-${version}"\

curl  -k -F "the_file=@${SIGNED_FILE_TMP}" https://jason:foo@firmware.globelock.home/upload.php -F "name=${project}-${version}"
echo
echo Uplaoded Version: ${version}

sleep 5
echo upgrade |nc lora-base.globelock.home 23 --send-on
echo upgrade |nc lora-base.globelock.home 23 --send-on