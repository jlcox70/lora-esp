
#ifndef BUILD_NUMBER
  #define BUILD_NUMBER "373"
#endif
#ifndef VERSION
  #define VERSION "lora_esp-v2023.12.373 - 2023-12-19 09:01:06.667002"
#endif
#ifndef VERSION_SHORT
  #define VERSION_SHORT "lora_esp-v2023.12.373"
#endif
